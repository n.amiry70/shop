from operator import ne
from unicodedata import name
from django.shortcuts import render
from django.http import HttpResponse
from .models import Product
from .forms import ProductForm, RawProductForm


# Create your views here. 
# /products
def index(request):
    products= Product.objects.all()
    return render(request, 'index.html', {'products':products})
    
# def new(request):
#     return HttpResponse('new')
    
def product_detail(request):
    product= Product.objects.get(id=1)
    context ={
        # 'title': product.name,
        # 'desc': product.price
        'obj': product
    }
    return render(request, 'detail.html', context)


def product_create(request):
    # form = ProductForm(request.POST or None)
    # if form.is_valid():
    #     form.save()
    #     form = ProductForm()
        
    # context ={
    #     'form' : form
    # }
    # or
    new_title = request.post.get('name')
    # Product.objects.create(name=new_title)
    # context ={}

    #or 
    myform= RawProductForm(request.post)
    context ={
        "form" : myform
    }
    return render(request, 'productCreate.html', context)