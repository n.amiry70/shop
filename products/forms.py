from email.mime import image
from .models import Product

from django import forms


class ProductForm(forms.ModelForm):
    class Meta:
        model =Product
        fields = [
            'name', 
            'stock',
            'price'
        ]
    
class RawProductForm(forms.Form):
    stock= forms.IntegerField()
    price= forms.FloatField()
    name= forms.DecimalField()
    image_url= forms.forms.CharField()
       