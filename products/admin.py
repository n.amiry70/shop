from itertools import product
from django.contrib import admin
from .models import Product,Offer



# manage midels in admin
class ProductAdmin(admin.ModelAdmin):
    list_display= ('name','price','stock')
    
class OfferAdmin(admin.ModelAdmin):
    list_display= ('code','discount')

# Register your models here.
admin.site.register(Offer,OfferAdmin)
admin.site.register(Product,ProductAdmin)