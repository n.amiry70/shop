from django.urls import path
# from .views import views
from .views import index, product_detail, product_create


urlpatterns = [
    path('', index),
    path('create', product_create),
    path('detail', product_detail),
]
