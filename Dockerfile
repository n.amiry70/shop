# Dockerfile

# base image
FROM django

# create & set working directory
RUN mkdir -p /usr/src
WORKDIR /usr/src

# copy source files
COPY . /usr/src

# install dependencies
RUN pip install

# start app
RUN python manage.py runserver
EXPOSE 3000
CMD python manage.py runserver